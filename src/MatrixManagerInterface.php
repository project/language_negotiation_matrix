<?php

namespace Drupal\language_negotiation_matrix;

interface MatrixManagerInterface {

  /**
   * Method to check if the Site Alias Prefix exists.
   *
   * This check will return a boolean value accordingly.
   *
   * @param array $mapping
   * @param string $language_code
   *
   * @return boolean
   */
  public function siteAliasExists(array $mapping, string $language_code);

  /**
   * This method returns the site alias maps.
   *
   * @param mixed $value
   *
   * @return array
   */
  public function getSiteAliasMapping($value);

  /**
   * This method will build the full site alias path.
   *
   * @param string $alias
   *
   * @return string
   */
  public function buildSiteAlias(string $alias);
}