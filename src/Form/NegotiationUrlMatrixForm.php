<?php

namespace Drupal\language_negotiation_matrix\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\language_negotiation_matrix\MatrixManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\language_negotiation_matrix\Plugin\LanguageNegotiation\LanguageNegotiationUrlMatrix;

/**
 * Configure the URL language negotiation method for this site.
 *
 * @internal
 */
class NegotiationUrlMatrixForm extends ConfigFormBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The Matrix manager.
   *
   * @var \Drupal\language_negotiation_matrix\MatrixManagerInterface
   */
  protected $matrixManager;

  /**
   * Constructs a new NegotiationUrlForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\language_negotiation_matrix\MatrixManagerInterface $matrix_manager
   *   The matrix manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager, MatrixManagerInterface $matrix_manager) {
    parent::__construct($config_factory);
    $this->languageManager = $language_manager;
    $this->matrixManager = $matrix_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('language_manager'),
      $container->get('language_negotiation_matrix.matrix_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'language_negotiation_configure_matrix_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['language.negotiation'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    global $base_root;
    $config = $this->config('language.negotiation');
    $module_handler = \Drupal::moduleHandler();

    // Get the form values and raw input (unvalidated values).
    $values = $form_state->getValues();

    // Define a wrapper id to populate new content into.
    $ajax_wrapper = 'matrix-ajax-wrapper';

    $form['language'] = [
      '#type' => 'details',
      '#tree' => TRUE,
      '#title' => $this->t('Language Configuration'),
      '#open' => TRUE,
    ];

    if($module_handler->moduleExists('key')){
      $options['key'] = $this->t('Key');
    }
    $options['json'] = $this->t('JSON');

    $form['language']['language_negotiation_matrix'] = [
      '#title' => $this->t('Matrix Definition'),
      '#type' => 'select',
      '#empty_value' => '',
      '#empty_option' => '- Select -',
      '#description' => $this->t('This language negotiation configuration can accept two ways of implementing the matrix: via a KEY using the <strong>key</strong> module [%enabled] or through a JSON object.',
        ['%enabled' => $module_handler->moduleExists('key') ? 'Enabled' : 'Not Installed']
      ),
      '#options' => $options,
      '#default_value' => $config->get('matrix.language.language_negotiation_matrix'),
      '#ajax' => [
        'callback' => [$this, 'ajaxSelectChange'],
        'event' => 'change',
        'wrapper' => $ajax_wrapper,
      ],
    ];
   if ($module_handler->moduleExists('key')){
      $form['language']['language_matrix'] = [
        '#type' => 'key_select',
        '#title' => $this->t('Language Matrix ENV variable'),
        '#description' => $this->t('To take advantage of this language detection you will require an ENVIRONMENT variable describing the language to path relationship. This will be consumed by the API and converted into a matrix for proper language switching. This functionality is born from a need to have the language and paths be supplied at runtime and likely highly customizable on a per project basis. This module will do its best to map the matrix in an intelligent way. It is possible to configure and export the results for more flexibility with Drupal.'),
        '#key_filters' => ['provider' => 'env', 'group' => 'generic'],
        '#states' => [
          'visible' => [
            ':input[name="language[language_negotiation_matrix]"]' => [
              'value' => (string) 'key',
            ],
          ],
        ],
      ];
    }

    $form['language']['language_matrix_json'] = [
      '#type' => 'codemirror',
      '#title' => $this->t('Language Matrix JSON'),
      '#description' => $this->t('Provide the matrix in JSON. To get the mappings to work, please SUBMIT the form and continue applying your settings.'),
      '#mode' => 'json',
      '#default_value' => $config->get('matrix.language.language_matrix_json'),
      '#states' => [
        'visible' => [
          ':input[name="language[language_negotiation_matrix]"]' => [
            'value' => (string) 'json',
          ],
        ],
      ],
    ];

    $form['language_negotiation_url_matrix_type'] = [
      '#title' => $this->t('Part of the URL that determines language'),
      '#type' => 'radios',
      '#options' => [
        LanguageNegotiationUrlMatrix::CONFIG_PATH_PREFIX => $this->t('Path prefix'),
        LanguageNegotiationUrlMatrix::CONFIG_DOMAIN => $this->t('Domain'),
      ],
      '#default_value' => $config->get('matrix.source'),
    ];

    $element = $form_state->getTriggeringElement();
    if (empty($values)) {
      $matrix_definition = $config->get('matrix.language.language_negotiation_matrix');
    } else {
      $matrix_definition = $element['#value'];
    }

    if ($matrix_definition == 'key') {
      $language_matrix = \Drupal::service('key.repository')->getKey('language_matrix');
      if ($language_matrix) {
        $matrix = $language_matrix->getKeyValues();
      } else {
        $matrix = NULL;
      }
    }
    elseif ($matrix_definition == 'json' && $config->get('matrix.language.language_negotiation_matrix') == 'json') {
      $matrix = Json::decode($config->get('matrix.language.language_matrix_json'));
    }
    elseif ($matrix_definition == 'json') {
      $matrix = $form_state->getValue('language_matrix_json');
    }
    else {
      $matrix = NULL;
    }

    // Build a wrapper for the ajax response.
    $form['ajax_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $ajax_wrapper,
      ]
    ];

    if ((!empty($values) && !empty($values['language_negotiation_matrix'])) || !empty($matrix)) {
      $default_value = $config->get('matrix.prefixes.mapping');
      $languages = $this->languageManager->getLanguages();
      $prefixes = $config->get('matrix.prefixes');
      $domains = $config->get('matrix.domains');

      foreach ($matrix as $langcode => $site_path) {
        $site_path = '/' . ltrim($site_path, '/');
        $requested_path[$langcode] = $site_path;
        if (isset($languages[$langcode])) {
          $language_mapping[$site_path] = $languages[$langcode]->getId();
        } else {
          $language_mapping[$site_path] = $langcode . ' (uninstalled)';
        }
      }

      $form['ajax_container']['prefix'] = [
        '#type' => 'details',
        '#tree' => TRUE,
        '#title' => $this->t('Path prefix configuration'),
        '#open' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="language_negotiation_url_matrix_type"]' => [
              'value' => (string) LanguageNegotiationUrlMatrix::CONFIG_PATH_PREFIX,
            ],
          ],
        ],
      ];

      $form['ajax_container']['prefix']['mapping'] = [
        '#type' => 'mapping',
        '#title' => $this->t('Language Matrix'),
        '#description_display' => 'before',
        '#source' => $requested_path,
        '#source__title' => $this->t('Path Requested'),
        '#destination' => $language_mapping,
        '#destination__title' => $this->t('Language Mapping'),
        '#destination__description' => NULL,
        '#default_value' => $default_value,
        '#filter' => FALSE,
        '#description' => $this->t('The Language Matrix Negotiation will attempt to map the paths to <strong>active</strong> languages per the environment variable provided. If you need to modify this list, please choose the desired mapping and save.'),
        '#prefix' => '<div id="mappings-wrapper">',
        '#suffix' => '</div>',
        '#required' => TRUE,
      ];

      $form['ajax_container']['domain'] = [
        '#type' => 'details',
        '#tree' => TRUE,
        '#title' => $this->t('Domain configuration'),
        '#open' => TRUE,
        '#description' => $this->t('The domain names to use for these languages. <strong>Modifying this value may break existing URLs. Use with caution in a production environment.</strong> Example: Specifying "de.example.com" as language domain for German will result in a URL like "http://de.example.com/contact".'),
        '#states' => [
          'visible' => [
            ':input[name="language_negotiation_url_matrix_type"]' => [
              'value' => (string) LanguageNegotiationUrlMatrix::CONFIG_DOMAIN,
            ],
          ],
        ],
      ];

      $form['ajax_container']['domain']['mapping'] = [
        '#type' => 'mapping',
        '#title' => $this->t('Language Matrix'),
        '#description_display' => 'before',
        '#source' => $requested_path,
        '#source__title' => $this->t('Path Requested'),
        '#destination' => $language_mapping,
        '#destination__title' => $this->t('Language Mapping'),
        '#destination__description' => NULL,
        '#default_value' => $default_value,
        '#filter' => FALSE,
        '#description' => $this->t('The Language Matrix Negotiation will attempt to map the paths to <strong>active</strong> languages per the environment variable provided. If you need to modify this list, please choose the desired mapping and save.'),
        '#prefix' => '<div id="mappings-wrapper">',
        '#suffix' => '</div>',
        '#required' => TRUE,
      ];

      $form['ajax_container']['prefix']['set'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<h4>Active Languages for Prefixes</h4>'),
      ];
      $form['ajax_container']['domain']['set'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<h4>Active Languages for Domains</h4>'),
      ];

      foreach ($languages as $langcode => $language) {
        $t_args = ['%language' => $language->getName(), '%langcode' => $language->getId()];
        $form['ajax_container']['prefix'][$langcode] = [
          '#type' => 'textfield',
          '#title' => $language->isDefault() ? $this->t('%language (%langcode) path prefix (Default language)', $t_args) : $this->t('%language (%langcode) path prefix', $t_args),
          '#maxlength' => 64,
          '#default_value' => isset($prefixes[$langcode]) ? $prefixes[$langcode] : '',
          '#field_prefix' => $base_root . $requested_path[$langcode] .  '/',
        ];
        $form['ajax_container']['domain'][$langcode] = [
          '#type' => 'textfield',
          '#title' => $this->t('%language (%langcode) domain', ['%language' => $language->getName(), '%langcode' => $language->getId()]),
          '#maxlength' => 128,
          '#default_value' => isset($domains[$langcode]) ? $domains[$langcode] : '',
          '#field_suffix' => $requested_path[$langcode] .  '/',
        ];
      }

    }

    $form_state->setRedirect('language.negotiation');

    return parent::buildForm($form, $form_state);
  }

  /**
   * The callback function for when the `my_select` element is changed.
   *
   * What this returns will be replace the wrapper provided.
   */
  public function ajaxSelectChange(array $form, FormStateInterface $form_state) {
    // Return the element that will replace the wrapper (we return itself).
    return $form['ajax_container'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $languages = $this->languageManager->getLanguages();
    $prefix_type = $form_state->getValue('language_negotiation_url_matrix_type');

    switch ($prefix_type) {
      case LanguageNegotiationUrlMatrix::CONFIG_PATH_PREFIX:
        // Get the form values for PREFIX
        $form_values = $form_state->getValue('prefix');
        // Set up the mappings for later
        $mapping = $this->matrixManager->getSiteAliasMapping($form_values);
        if (isset($form_values)) {
          // Remove "mapping" array
          unset($form_values['mapping']);
          $count = array_count_values($form_values);
          // This step is to walk through the language array and implement the
          // necessary validation to ensure configuration won't fail.
          foreach ($languages as $langcode => $language) {
            $prefix_language = $form_state->getValue(['prefix', $langcode]);
            // First, we'll check to see if the language prefixes event exist.
            if ($prefix_language === '') {
              // stub -- may be used for future purpose.
            }
            elseif (strpos($prefix_language, '/') !== FALSE) {
              // Throw a form error if the string contains a slash,
              // which would not work.
              $form_state->setErrorByName("prefix][$langcode", $this->t('The prefix may not contain a slash.'));
            }
            elseif (isset($count[$prefix_language]) && $count[$prefix_language] > 1) {
              // Throw a form error if there are two languages with the same
              // domain/prefix.
              $form_state->setErrorByName("prefix][$langcode", $this->t('The prefix for %language, %value, is not unique.', ['%language' => $language->getName(), '%value' => $prefix_language]));
            }

            // Now that we've checked if we have any errors in the form values,
            // we need to check and see if the paths exist on the system.
            // Consider if using Apache or Nginx, some sort of symbolic links
            // and folder combination will permit drupal to render its site at
            // the proper URI. For this to work, the folder structure needs to
            // exist. Let us check if the symlinks and directories are present,
            // otherwise note the discrepancy.
            // @TODO: We may want to let this slide so bad prefixes CAN exist.
            if (!$this->matrixManager->siteAliasExists($mapping, $langcode)) {
              // Throw a form error if the sites alias (folder structure) doesn't exist.
              $form_state->setErrorByName("prefix][$langcode", $this->t('The folder %folder does not exist, therefore a proper matrix cannot yet be created. Please update your server configuration accordingly.', ['%folder' => $mapping[$langcode]]));
            }
          }
        }
        break;

      case LanguageNegotiationUrlMatrix::CONFIG_DOMAIN:
        // Get the form values for DOMAIN
        $form_values = $form_state->getValue('domain');
        if (isset($form_values)) {
          // remove "mapping" array
          unset($form_values['mapping']);
          $count = array_count_values($form_values);
          foreach ($languages as $langcode => $language) {
            $domain_language = $form_state->getValue(['domain', $langcode]);

            if ($domain_language === '') {
              if ($form_state->getValue('language_negotiation_url_matrix_type') == LanguageNegotiationUrlMatrix::CONFIG_DOMAIN) {
                // Throw a form error if the domain is blank for a non-default language,
                // although it is required for selected negotiation type.
                $form_state->setErrorByName("domain][$langcode", $this->t('The domain may not be left blank for %language.', ['%language' => $language->getName()]));
              }
            }
            elseif (isset($count[$domain_language]) && $count[$domain_language] > 1) {
              // Throw a form error if there are two languages with the same
              // domain/domain.
              $form_state->setErrorByName("domain][$langcode", $this->t('The domain for %language, %value, is not unique.', [
                '%language' => $language->getName(),
                '%value' => $domain_language
              ]));
            }
          }

          // Domain names should not contain protocol and/or ports.
          foreach ($languages as $langcode => $language) {
            $domain_language = $form_state->getValue(['domain', $langcode]);
            if (!empty($domain_language)) {
              // Ensure we have exactly one protocol when checking the hostname.
              $host = 'http://' . str_replace(['http://', 'https://'], '', $domain_language);
              if (parse_url($host, PHP_URL_HOST) != $domain_language) {
                $form_state->setErrorByName("domain][$langcode", $this->t('The domain for %language may only contain the domain name, not a trailing slash, protocol and/or port.', ['%language' => $language->getName()]));
              }
            }
          }
        }

        break;
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('language.negotiation');
    $prefix_type = $form_state->getValue('language_negotiation_url_matrix_type');

    switch ($prefix_type) {
      case LanguageNegotiationUrlMatrix::CONFIG_PATH_PREFIX:
        $mappings = $form_state->getValue('prefix');
        break;

      case LanguageNegotiationUrlMatrix::CONFIG_DOMAIN:
        $mappings = $form_state->getValue('domain');
        break;
    }

    if (!isset($mappings)) {
      $this->messenger()->addWarning($this->t('The %type values are not set properly. There is a good chance this Language Negotiation will not do what you want it to. Consider looking into the Matrix Definition and make sure the proper values are set or being passed in correctly.', [
        '%type' => strtoupper($prefix_type),
      ]));
      $config->set('matrix.has_path', FALSE);
    } else {
      if ($prefix_type == LanguageNegotiationUrlMatrix::CONFIG_PATH_PREFIX) {
        $config->set('matrix.has_path', TRUE);
      }
    }

    $config
      ->set('matrix.language', $form_state->getValue('language'))
      ->set('matrix.source', $prefix_type)
      ->set('matrix.prefixes', $form_state->getValue('prefix'))
      ->set('matrix.domains', $form_state->getValue('domain'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
