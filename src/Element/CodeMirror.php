<?php

namespace Drupal\language_negotiation_matrix\Element;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Serialization\Yaml as DrupalYaml;
use Drupal\Core\Render\Element\Textarea;
use Drupal\Core\Form\FormStateInterface;
use Drupal\language_negotiation_matrix\Utility\Yaml;

/**
 * Provides a form element for using CodeMirror.
 *
 * Known Issues/Feature Requests:
 *
 * - Mixed Twig Mode #3292
 *   https://github.com/codemirror/CodeMirror/issues/3292
 *
 * @FormElement("codemirror")
 */
class CodeMirror extends Textarea {

  /**
   * An associative array of supported CodeMirror modes by type and mime-type.
   *
   * @var array
   */
  protected static $modes = [
    'css' => 'text/css',
    'html' => 'text/html',
    'htmlmixed' => 'htmlmixed',
    'json' => 'text/javascript',
    'javascript' => 'text/javascript',
    'text' => 'text/plain',
    'yaml' => 'text/x-yaml',
    'php' => 'text/x-php',
  ];

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#mode' => 'text',
      '#skip_validation' => FALSE,
      '#decode_value' => FALSE,
      '#cols' => 60,
      '#rows' => 5,
      '#wrap' => TRUE,
      '#resizable' => 'vertical',
      '#process' => [
        [$class, 'processCodeMirror'],
        [$class, 'processAjaxForm'],
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderCodeMirror'],
        [$class, 'preRenderGroup'],
      ],
      '#theme' => 'textarea',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input === FALSE && $element['#mode'] === 'yaml' && isset($element['#default_value'])) {
      // Convert associative array in default value to YAML.
      if (is_array($element['#default_value'])) {
        $element['#default_value'] = Yaml::encode($element['#default_value']);
      }
      // Convert empty YAML into an empty string.
      if ($element['#default_value'] === '{  }') {
        $element['#default_value'] = '';
      }
      return $element['#default_value'];
    }
    return NULL;
  }

  /**
   * Processes a 'codemirror' element.
   */
  public static function processCodeMirror(&$element, FormStateInterface $form_state, &$complete_form) {
    // Check that mode is defined and valid, if not default to (plain) text.
    if (empty($element['#mode']) || !isset(static::$modes[$element['#mode']])) {
      $element['#mode'] = 'text';
    }

    // Set wrap off.
    if (empty($element['#wrap'])) {
      $element['#attributes']['wrap'] = 'off';
    }

    // Add validate callback.
    $element += ['#element_validate' => []];
    array_unshift($element['#element_validate'], [get_called_class(), 'validateCodeMirror']);

    return $element;
  }

  /**
   * Prepares a #type 'code' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderCodeMirror(array $element) {
    static::setAttributes($element, ['js-codemirror', 'codemirror', $element['#mode']]);
    $element['#attributes']['data-codemirror-mode'] = static::getMode($element['#mode']);
    $element['#attached']['library'][] = 'language_negotiation_matrix/element.codemirror.' . $element['#mode'];
    return $element;
  }

  /**
   * Form element validation handler for #type 'codemirror'.
   */
  public static function validateCodeMirror(&$element, FormStateInterface $form_state, &$complete_form) {
    // If element is disabled then use the #default_value.
    if (!empty($element['#disable'])) {
      $element['#value'] = $element['#default_value'];
      $form_state->setValueForElement($element, $element['#default_value']);
    }
    $errors = static::getErrors($element, $form_state, $complete_form);
    if ($errors) {
      $build = [
        'title' => [
          '#markup' => t('%title is not valid.', ['%title' => static::getTitle($element)]),
        ],
        'errors' => [
          '#theme' => 'item_list',
          '#items' => $errors,
        ],
      ];
      $form_state->setError($element, \Drupal::service('renderer')->render($build));
    }
    else {
      // If editing YAML and #default_value is an array, decode #value.
      if ($element['#mode'] === 'yaml'
        && (isset($element['#default_value']) && is_array($element['#default_value']) || $element['#decode_value'])
      ) {
        // Handle rare case where single array value is not parsed correctly.
        if (preg_match('/^- (.*?)\s*$/', $element['#value'], $match)) {
          $value = [$match[1]];
        }
        else {
          $value = $element['#value'] ? DrupalYaml::decode($element['#value']) : [];
        }
        $form_state->setValueForElement($element, $value);
      }
    }
  }

  /**
   * Get validation errors.
   */
  protected static function getErrors(&$element, FormStateInterface $form_state, &$complete_form) {
    if (!empty($element['#skip_validation'])) {
      return NULL;
    }

    switch ($element['#mode']) {
      case 'json':
        return static::validateJson($element, $form_state, $complete_form);

      case 'html':
        return static::validateHtml($element, $form_state, $complete_form);

      case 'yaml':
        return static::validateYaml($element, $form_state, $complete_form);

      case 'twig':
        return static::validateTwig($element, $form_state, $complete_form);

      default:
        return NULL;
    }
  }

  /**
   * Get an element's title.
   *
   * @param array $element
   *   An element.
   *
   * @return string
   *   The element's title.
   */
  protected static function getTitle(array $element) {
    if (isset($element['#title'])) {
      return $element['#title'];
    }

    switch ($element['#mode']) {
      case 'json':
        return t('JSON');

      case 'html':
        return t('HTML');

      case 'yaml':
        return t('YAML');

      default:
        return t('Code');
    }
  }

  /**
   * Get the CodeMirror mode for specified type.
   *
   * @param string $mode
   *   Mode (text, html, or yaml).
   *
   * @return string
   *   The CodeMirror mode (aka mime type).
   */
  public static function getMode($mode) {
    return (isset(static::$modes[$mode])) ? static::$modes[$mode] : static::$modes['text'];
  }

  /****************************************************************************/
  // Language/markup validation callback.
  /****************************************************************************/

  /**
   * Validate HTML.
   *
   * @param array $element
   *   The form element whose value is being validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array|null
   *   An array of error messages.
   */
  protected static function validateHtml($element, FormStateInterface $form_state, $complete_form) {
    // @see: http://stackoverflow.com/questions/3167074/which-function-in-php-validate-if-the-string-is-valid-html
    // @see: http://stackoverflow.com/questions/5030392/x-html-validator-in-php
    libxml_use_internal_errors(TRUE);
    if (simplexml_load_string('<fragment>' . $element['#value'] . '</fragment>')) {
      return NULL;
    }

    $errors = libxml_get_errors();
    libxml_clear_errors();
    if (!$errors) {
      return NULL;
    }

    $messages = [];
    foreach ($errors as $error) {
      $messages[] = $error->message;
    }
    return $messages;
  }

  /**
   * Validate YAML.
   *
   * @param array $element
   *   The form element whose value is being validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array|null
   *   An array of error messages.
   */
  protected static function validateYaml($element, FormStateInterface $form_state, $complete_form) {
    try {
      $value = $element['#value'];
      $data = DrupalYaml::decode($value);
      if (!is_array($data) && $value) {
        throw new \Exception(t('YAML must contain an associative array of elements.'));
      }
      return NULL;
    }
    catch (\Exception $exception) {
      return [$exception->getMessage()];
    }
  }

  /**
   * Validate JSON.
   *
   * @param array $element
   *   The form element whose value is being validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array|null
   *   An array of error messages.
   */
  protected static function validateJson($element, FormStateInterface $form_state, $complete_form) {
    try {
      $value = $element['#value'];
      $data = Json::decode($value);
      if (!is_array($data) && $value) {
        switch (json_last_error()) {
          case JSON_ERROR_NONE:
            $message = t(' - No errors');
            break;
          case JSON_ERROR_DEPTH:
            $message = t(' - Maximum stack depth exceeded');
            break;
          case JSON_ERROR_STATE_MISMATCH:
            $message = t(' - Underflow or the modes mismatch');
            break;
          case JSON_ERROR_CTRL_CHAR:
            $message = t(' - Unexpected control character found');
            break;
          case JSON_ERROR_SYNTAX:
            $message = t(' - Syntax error, malformed JSON');
            break;
          case JSON_ERROR_UTF8:
            $message = t(' - Malformed UTF-8 characters, possibly incorrectly encoded');
            break;
          default:
            $message = t(' - Unknown error');
            break;
        }
        throw new \Exception($message);
      }
      return NULL;
    }
    catch (\Exception $exception) {
      return [$exception->getMessage()];
    }
  }

}
