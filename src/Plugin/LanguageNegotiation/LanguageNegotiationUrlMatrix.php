<?php

namespace Drupal\language_negotiation_matrix\Plugin\LanguageNegotiation;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Drupal\language\Annotation\LanguageNegotiation;
use Drupal\language\LanguageNegotiationMethodBase;
use Drupal\language\LanguageSwitcherInterface;
use Drupal\language_negotiation_matrix\MatrixManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for identifying language via URL prefix or domain.
 *
 * @LanguageNegotiation(
 *   id = \Drupal\language_negotiation_matrix\Plugin\LanguageNegotiation\LanguageNegotiationUrlMatrix::METHOD_ID,
 *   types = {\Drupal\Core\Language\LanguageInterface::TYPE_INTERFACE,
 *   \Drupal\Core\Language\LanguageInterface::TYPE_CONTENT,
 *   \Drupal\Core\Language\LanguageInterface::TYPE_URL},
 *   weight = -99,
 *   name = @Translation("URL Matrix"),
 *   description = @Translation("Language from the URL Matrix (Path prefix and/or domain)."),
 *   config_route_name = "language_negotiation_matrix.negotiation_url_matrix"
 * )
 */
class LanguageNegotiationUrlMatrix extends LanguageNegotiationMethodBase implements InboundPathProcessorInterface, OutboundPathProcessorInterface, LanguageSwitcherInterface, ContainerFactoryPluginInterface {

  /**
   * The language negotiation method id.
   */
  const METHOD_ID = 'language-url-matrix';

  /**
   * URL language negotiation: use the path prefix as URL language indicator.
   */
  const CONFIG_PATH_PREFIX = 'path_prefix';

  /**
   * URL language negotiation: use the domain as URL language indicator.
   */
  const CONFIG_DOMAIN = 'domain_matrix';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language matrix manager.
   *
   * @var \Drupal\language_negotiation_matrix\MatrixManager
   */
  protected $matrixManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new MatrixManager instance.
   *
   * @param \Drupal\language_negotiation_matrix\MatrixManager $matrix_manager
   *   The matrix manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(MatrixManagerInterface $matrix_manager, LoggerInterface $logger) {
    $this->matrixManager = $matrix_manager;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('language_negotiation_matrix.matrix_manager'),
      $container->get('logger.factory')->get('language_negotiation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL) {
    $langcode = NULL;

    if ($request && $this->languageManager) {
      $languages = $this->languageManager->getLanguages();
      $config = $this->config->get('language.negotiation')->get('matrix');
      $state = \Drupal::state()->get('language_negotiation_matrix');
      if (!isset($config['source'])) {
        if (!$state) {
          $this->logger->warning('Language Negotiation URL Matrix configuration is not complete. Please consider adding a source type.');
          \Drupal::state()->set('language_negotiation_matrix', TRUE);
        }
        return NULL;
      }

      switch ($config['source']) {
        case LanguageNegotiationUrlMatrix::CONFIG_PATH_PREFIX:
          $request_path = urldecode(trim($request->getPathInfo(), '/'));
          $path_args = explode('/', $request_path);
          $prefix = array_shift($path_args);

          // Search prefix within added languages.
          $negotiated_language = FALSE;
          foreach ($languages as $language) {
            if (isset($config['prefixes'][$language->getId()]) && $config['prefixes'][$language->getId()] == $prefix) {
              $negotiated_language = $language;
              break;
            }
          }

          if ($negotiated_language) {
            $langcode = $negotiated_language->getId();
          }
          break;

        case LanguageNegotiationUrlMatrix::CONFIG_DOMAIN:
          // Get only the host, not the port.
          $http_host = $request->getHost();
          foreach ($languages as $language) {
            // Skip the check if the language doesn't have a domain.
            if (!empty($config['domains'][$language->getId()])) {
              // Ensure that there is exactly one protocol in the URL when
              // checking the hostname.
              $host = 'http://' . str_replace(['http://', 'https://'], '', $config['domains'][$language->getId()]);
              $host = parse_url($host, PHP_URL_HOST);
              if ($http_host == $host) {
                $langcode = $language->getId();
                break;
              }
            }
          }
          break;
      }
    }

    return $langcode;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    $config = $this->config->get('language.negotiation')->get('matrix');

    if ($config['source'] == LanguageNegotiationUrlMatrix::CONFIG_PATH_PREFIX) {
      $parts = explode('/', trim($path, '/'));
      $prefix = array_shift($parts);

      // Search prefix within added languages.
      foreach ($this->languageManager->getLanguages() as $language) {
        if (isset($config['prefixes'][$language->getId()]) && $config['prefixes'][$language->getId()] == $prefix) {
          // Rebuild $path with the language removed.
          $path = '/' . implode('/', $parts);
          break;
        }
      }
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    global $base_root;
    $url_scheme = 'http';
    $port = 80;
    if ($request) {
      $url_scheme = $request->getScheme();
      $port = $request->getPort();
    }
    $languages = array_flip(array_keys($this->languageManager->getLanguages()));
    // Language can be passed as an option, or we go for current URL language.
    if (!isset($options['language'])) {
      $language_url = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_URL);
      $options['language'] = $language_url;
    }
    // We allow only added languages here.
    elseif (!is_object($options['language']) || !isset($languages[$options['language']->getId()])) {
      return $path;
    }

    $config = $this->config->get('language.negotiation')->get('matrix');
    $langcode = $options['language']->getId();

    // Update the PATH PREFIX mapping
    if ($config['source'] == LanguageNegotiationUrlMatrix::CONFIG_PATH_PREFIX) {
      if (is_object($options['language'])) {
        if (!empty($config['prefixes'][$langcode])) {
          $options['prefix'] = $config['prefixes'][$langcode] . '/';
        }
        $mapping = $this->matrixManager->getSiteAliasMapping($config['prefixes']);
        if (!empty($mapping) && $this->matrixManager->siteAliasExists($mapping, $langcode)) {
          $dir = $mapping[$langcode];
          $options['base_url'] = $base_root . $dir;
          $options['absolute'] = TRUE;
        }
        if ($bubbleable_metadata) {
          $bubbleable_metadata->addCacheContexts(['languages:' . LanguageInterface::TYPE_URL, 'url.site']);
        }
      }
    }
    // Update the DOMAIN PREFIX mapping
    elseif ($config['source'] == LanguageNegotiationUrlMatrix::CONFIG_DOMAIN) {

      if ($request) {
        $requested_path = rtrim($request->getRequestUri(), '/');
      }

      if (is_object($options['language']) && !empty($config['domains'][$langcode])) {

        if (!empty($mapping = $this->matrixManager->getSiteAliasMapping($config['domains']))) {
          $mapped_paths = array_flip($mapping);
        }

        // Save the original base URL. If it contains a port, we need to
        // retain it below.
        if (!empty($options['base_url'])) {
          // The colon in the URL scheme messes up the port checking below.
          $normalized_base_url = str_replace(['https://', 'http://'], '', $options['base_url']);
        }

        // Ask for an absolute URL with our modified base URL.
        $options['absolute'] = TRUE;

        if (($request) && isset($mapped_paths[$requested_path]) && isset($language_url) && ($language_url->getId() != $mapped_paths[$requested_path])) {
          $options['base_url'] = $url_scheme . '://' . $config['domains'][$mapped_paths[$requested_path]];
          $dir = $config['domains']['mapping'][$mapped_paths[$requested_path]];
        } elseif (($request) && isset($config['domains']['mapping'][$langcode])) {
          $options['base_url'] = $url_scheme . '://' . $config['domains'][$langcode];
          $dir = $config['domains']['mapping'][$langcode];
        } else {
          $options['base_url'] = $base_root;
          $dir = '';
        }

        // In case either the original base URL or the HTTP host contains a
        // port, retain it.
        if (isset($normalized_base_url) && strpos($normalized_base_url, ':') !== FALSE) {
          [, $port] = explode(':', $normalized_base_url);
          $options['base_url'] .= ':' . $port;
        }
        elseif (($url_scheme == 'http' && $port != 80) || ($url_scheme == 'https' && $port != 443)) {
          $options['base_url'] .= ':' . $port;
        }

        if (isset($options['https'])) {
          if ($options['https'] === TRUE) {
            $options['base_url'] = str_replace('http://', 'https://', $options['base_url']);
          }
          elseif ($options['https'] === FALSE) {
            $options['base_url'] = str_replace('https://', 'http://', $options['base_url']);
          }
        }

        $options['base_url'] .= $dir;

        if ($bubbleable_metadata) {
          $bubbleable_metadata->addCacheContexts(['languages:' . LanguageInterface::TYPE_URL, 'url.site']);
        }
      }
    }

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getLanguageSwitchLinks(Request $request, $type, Url $url) {
    $links = [];
    $query = $request->query->all();

    foreach ($this->languageManager->getNativeLanguages() as $language) {
      $links[$language->getId()] = [
        // We need to clone the $url object to avoid using the same one for all
        // links. When the links are rendered, options are set on the $url
        // object, so if we use the same one, they would be set for all links.
        'url' => clone $url,
        'title' => $language->getName(),
        'language' => $language,
        'attributes' => ['class' => ['language-link']],
        'query' => $query,
      ];
    }

    return $links;
  }

}
