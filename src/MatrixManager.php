<?php

namespace Drupal\language_negotiation_matrix;

use Drupal\Core\DrupalKernel;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The Matrix Manager class.
 *
 * We use this class to provide methods for language negotiation matrix
 * manager utilities.
 */
class MatrixManager implements MatrixManagerInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructor for the MatrixManager.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Core\File\FileSystemInterface|null $file_system
   */
  public function __construct(ModuleHandlerInterface $module_handler, FileSystemInterface $file_system = NULL) {
    $this->moduleHandler = $module_handler;
    $this->fileSystem = $file_system;
  }

  /**
   * @inheritDoc
   */
  public function siteAliasExists(array $mapping, $language_code) {
    if (!empty($mapping) && isset($mapping[$language_code])) {
      $path = $this->buildSiteAlias($mapping[$language_code]);
      $path_exists = is_dir($path);
      return $path_exists;
    }
    return FALSE;
  }

  /**
   * @inheritDoc
   */
  public function buildSiteAlias(string $alias) {
    return DRUPAL_ROOT . $alias;
  }

  /**
   * @inheritDoc
   */
  public function getSiteAliasMapping($value) {
    if (isset($value['mapping'])) {
      unset($value['mapping']['table']);
      return $value['mapping'];
    }
    return [];
  }

}