# Language Negotiation: URL Matrix

This module enhances Drupal Language Negotiation URLs. By providing a way to map languages to paths or domains in a way that does not confuse the User when using the Language Switcher.

Consider the following examples:

## Example #1: Same Domain, different paths

You have a site which exists at a domain which is multilingual by virtue of its name, say, `someplace.earth`. Now you have a set of paths which coexist under this domain for the same site but are different in various languages:

- `someplace.earth/sites/english-wording/en`
- `someplace.earth/sites/wording-in-french/fr`

In the above example, we can see quite a few elements of the URI are similar, but the subtle differences exist in the language identifier and the segment in french. If we had a normal Language Negotiation going on, a visitor to the site would actually see the following in the language switcher if they visited the english side of the site first:

- `someplace.earth/sites/english-wording/en`
- `someplace.earth/sites/english-wording/fr`

The reason for this stems from HOW the Language Negotiation works. Drupal really only understands the one domain and the one path. But, we know we can have several paths per entity, but that does not define the prefix of a path. Everything before the language identifier (`en` or `fr`) can only be set via your server setup or the paths you've defined outside of drupal. This means if a visitor follows links to the following:

- `someplace.earth/sites/wording-in-french/fr`

The english choice would read:

- `someplace.earth/sites/wording-in-french/en`

This is sub-optimal. How to avoid this scenario? You need to setup some language negotiation PathProcessing which modifies the path prefix BEFORE Drupal has a chance to work with it. So what we need is to develop a matrix of path prefixes to language identifiers and then permit the language switcher to leverage this matrix.

The final result as above would look something like this:

- `someplace.earth/sites/english-wording/en` --> EN
- `someplace.earth/sites/wording-in-french/en` remaps EN

- `someplace.earth/sites/wording-in-french/fr` --> FR
- `someplace.earth/sites/english-wording/fr` remaps FR

In terms of providing a useful UI/UX, the module will extend the current administrative configuration for language negotiation of type 'URL'. The configuration for each language will have an option to set one or more prefixes used by the site. These will be programmatically ascertained from the site settings OR specifically set via environment variables (I lean toward these).


## Example #2: Different Domain, different paths

It's quite possible that a project requires a multilingual solution where the configuration of a domain will be such that each language has a different domain. This is not ideal for several reasons, but isn't unheard of. The request is more common than one might think. So, how do we get around this without adding all sorts of complications?

Language Negotiation to the rescue! Let's imagine this scenario:

- `eng.someplace.earth/sites/english-wording`
- `fra.someplace.earth/sites/wording-in-french`

or even

- `eng.someplace.earth/sites/same-in-all-languages`
- `fra.someplace.earth/sites/same-in-all-languages`

In both above scenarios, the domain is different per language but respects the cookie domain requirement being that the root domain is identical. Should the domains be completely different, you'll have another fundamental issue with holding on to User Sessions as cross-domain cookies are not permitted. But that's entirely another story.

Cross-Domain Cookies aside, this module will extend the Domain Language Negotiation configuration to provide language mapping on a per domain basis BUT also allow the matrix to exist with the URI sub-path.

## Exceptions: same sub-paths

Fundamentally, this module won't give you any real benefit, since the normal Language Negotiation URL using the Domain option would perform the same thing. For the same of completeness, we've rolled the same functionality in.

## Installation

Follow all normal methods for installing and enabling this module via composer.

## Configuration

When using paths and domains which force the drupal installation cookies to work with complex pathing, you will have to modify the paramters in the `services.yml` file:

```yaml
parameters:
  session.storage.options:
    cookie_domain: '.localhost'
    cookie_samesite: 'Lax'
```
Note: If using a wildcard `.` to denote "anything before the domain", if you are using "domain matrix" the cookie may not work as intended. Either explicitly mark the domain or subdomain component which the cookie will adhere to otherwise user sessions won't work from one language to another.

## Example JSON object for language matrix

To continue with the scenari above, an example JSON object to send to the module might look like this:

```json
{
  "en": "/site/english",
  "fr": "/site/francais"
}
```

The above JSON would tell the module that all `en` URLS are to be mapped to the path `/site/english`. And subsequently all the `fr` URLS are to be mapped to `/site/francais`. This would ensure that the language switched doesn't end up making weird combinations and causing confusion between paths and domains for the end user.